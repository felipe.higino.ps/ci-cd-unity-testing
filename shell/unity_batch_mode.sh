#!/usr/bin/env bash

#--------------- 
#build target platform
# BUILD_TARGET=Standalone
BUILD_TARGET=Win
# BUILD_TARGET=Win64
# BUILD_TARGET=OSXUniversal
# BUILD_TARGET=Linux64
# BUILD_TARGET=iOS
# BUILD_TARGET=Android
# BUILD_TARGET=WebGL
# BUILD_TARGET=WindowsStoreApps
# BUILD_TARGET=tvOS

#--------------- 
#unity version, installed in machine
UNITY_EXECUTABLE="/mnt/c/Program Files/Unity/Hub/Editor/2021.3.0f1/Editor/Unity.exe"

#project path. if using git, is the cloned one
PROJECT_PATH="C:/Projetos Unity/Monomyto/ci-cd-unity-test/"

#log file, registering build status
LOG_PATH="C:/Projetos Unity/Monomyto/ci-cd-unity-test/shell/logs-build.txt"

#--------------- 
#   -quit \
#   -batchmode \
#   -nographics \
# unity batch mode command
"$UNITY_EXECUTABLE" \
  -buildTarget $BUILD_TARGET \
  -projectPath "$PROJECT_PATH" \
  -executeMethod BuildClass.Build \
  -logFile "$LOG_PATH"

#--------------- 
#catching possible errors
UNITY_EXIT_CODE=$?

if [ $UNITY_EXIT_CODE -eq 0 ]; then
  echo "Run succeeded, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 2 ]; then
  echo "Run succeeded, some tests failed";
elif [ $UNITY_EXIT_CODE -eq 3 ]; then
  echo "Run failure (other failure)";
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
fi
