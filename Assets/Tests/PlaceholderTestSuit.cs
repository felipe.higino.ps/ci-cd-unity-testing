using System.Collections;
using System.Linq;

using UnityEngine;
using UnityEngine.TestTools;

using NUnit.Framework;
using System;

public class PlaceholderTestSuit
{
    [Test]
    public void SimpleTest()
    {
        int sum = 1 + 1;
        Assert.That(sum == 2);
    }

    [Test]
    public void EnvironmentFail()
    {
        var shouldFail = Environment.GetEnvironmentVariable("shouldfail");
        Assert.That(shouldFail != "true");
    }
}
