using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;
using UnityEngine.Networking;

public class DisplayStreamingAssetExample : MonoBehaviour
{
    [SerializeField] string streamingAssetName;
    [SerializeField] TMP_Text textTMPro;

    private void Awake()
    {
        StartCoroutine(nameof(GetFileRoutine));
    }

    private IEnumerator GetFileRoutine()
    {
        textTMPro.text = "loading asset...";

        string assetPath = Path.Combine(Application.streamingAssetsPath, streamingAssetName);

        UnityWebRequest loadingRequest = UnityWebRequest.Get(assetPath);
        loadingRequest.SendWebRequest();

        while (!loadingRequest.isDone)
        {
            yield return null;
        }

        textTMPro.text = loadingRequest.downloadHandler.text;
    }
}
