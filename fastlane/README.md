fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## iOS

### ios testing

```sh
[bundle exec] fastlane ios testing
```



### ios setup_projects

```sh
[bundle exec] fastlane ios setup_projects
```



### ios setup_services

```sh
[bundle exec] fastlane ios setup_services
```



### ios setup_unity_iphone_xcodeproj

```sh
[bundle exec] fastlane ios setup_unity_iphone_xcodeproj
```



### ios setup_pods_xcodeproj

```sh
[bundle exec] fastlane ios setup_pods_xcodeproj
```



----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
