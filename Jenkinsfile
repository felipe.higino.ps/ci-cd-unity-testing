// workspace C:\ProgramData\Jenkins\.jenkins\workspace\
// good tutorial https://www.jenkins.io/doc/book/pipeline/jenkinsfile/
// using jenkinsfile https://www.jenkins.io/doc/book/pipeline/jenkinsfile/
// see archiveArtifacts
// see stash

//--------------------------------------------- functions
String GetAgentLabel(String agentOS){
    return "${agentOS}-agent"
}

String RunUnityPlaymodeTests(){
    return "${env.unityExecutable} -runTests -batchmode -projectPath ${env.unityProjectPath} -testResults ${env.testsXMLReport} -testPlatform PlayMode"
}

String BuildUnityPlayerBatchmode(){
    return "${env.unityExecutable} -quit -batchmode -projectPath ${env.unityProjectPath} -executeMethod ${env.unityMethod} -logfile -"
}

//--------------------------------------------- pipeline setup functions

def GetAvaiableAgentsOS(){
    def agents = ['windows', 'mac']
    def avaiableAgents = []

    for (agentOS in agents) {
        def agentLabel = GetAgentLabel(agentOS)
        def nodes = nodesByLabel label: agentLabel
        def existNode = nodes.size() > 0
        if(existNode){
            avaiableAgents << agentOS
        }
    }
    
    return avaiableAgents
}

def SetupUnityScriptEnvironmentVariables(String _platform){
    // captured through unity build script
    env.buildName="Gunstars-${_platform}-v0.0.0-build${env.BUILD_NUMBER}"
    env.artifactsPath="${env.WORKSPACE}\\Artifacts\\${env.buildName}"
    env.unityBuildAsset='test-il2cpp-lowstrip-arm32arm64-aab'

    // nunit test report
    env.testsXMLReport="Artifacts/${env.buildName}/TestResult-${env.buildName}.xml"
}

def SetupUnityScriptEnvironmentVariablesMAC(String _platform){
    // captured through unity build script
    env.buildName="Gunstars-${_platform}-v0.0.0-build${env.BUILD_NUMBER}"
    env.artifactsPath="${env.WORKSPACE}/Artifacts/${env.buildName}"
    // env.unityBuildAsset="Gunstars-mac-${_platform}"
    env.unityBuildAsset='Gunstars-mac-ios-ipa'

    // nunit test report
    env.testsXMLReport="Artifacts/${env.buildName}/TestResult-${env.buildName}.xml"
}


//--------------------------------------------- pipeline options selection

properties(
    [
        parameters([
                choice(
                    name: "BUILD_SCRIPT",
                    choices: [
                        'App distribution', 
                        'Install dependencies', 
                        'Test'
                    ],
                    description: 'The predefined command to run'
                ),
                booleanParam(
                    name: 'LOCAL_FOLDER', 
                    defaultValue: true, 
                    description: 'Should use local project?'
                ),
        ])   
    ]
)

//--------------------------------------------- workspace switch

if(params.LOCAL_FOLDER){
    Workspace_LocalFolder()
}
else{
    Workspace_GitSCM()
}

def Workspace_LocalFolder(){
    // node(){
    //     ws("C:\\Projetos Unity\\Monomyto\\gunstars-android-copy"){
    //         EnterPoint()
    //     }
    // }
    env.WORKSPACE = "C:\\Projetos Unity\\Monomyto\\gunstars-android-copy"
    EnterPoint()
}

def Workspace_GitSCM(){
    EnterPoint()
}

//--------------------------------------------- Enter point

def EnterPoint(){
    HardcodedVariables()
    CommandSwitch()
}

def HardcodedVariables(){
    env.FASTLANE_RELATIVE_PATH = "MonomytoSystems/CICD/fastlane-setup"
}

def CommandSwitch(){
    switch(params.BUILD_SCRIPT) {
        case 'App distributiion':
            AgentSwitch()
            break;
        case 'Install dependencies':
            InstallDependencies()
            break;
        case 'Test':
            Test()
            break;
        break
    }
}

//--------------------------------------------- Commands enter points

def AgentSwitch(){
    for(agent in GetAvaiableAgentsOS()){
        switch(agent) {
            case "windows":
                WindowsBuilderPipeline()
                break

            case "mac":
                MACOSBuilderPipeline()
                break

            default:
                echo 'unknown agent, passing trough...'
                break
        }
    }
}

def InstallDependencies(){

}

def Test(){
    node(){
        ws(env.WORKSPACE){
            stage('Test'){
                env.TESTING_ENV = "testing env mudado pelo jenkins"
                dir(env.FASTLANE_RELATIVE_PATH) {
                    bat 'bundle exec fastlane testing'
                }
            }
        }
    }
}

//--------------------------------------------- WINDOWS BUILDER
def WindowsBuilderPipeline(){
    echo 'starting build in Windows agent...'
    // def builderPlatforms = ['android', 'steam', 'switch']
    def builderPlatforms = ['android']

    node(GetAgentLabel('windows')){

        stage('Checkout'){
            echo "( o_o)---- Sync git repository..."
            checkout scm
        }

        stage('ENV Builder'){
            echo "( o_o)---- Add Builder Environment variables..."
            env.unityExecutable="\"C:\\Program Files\\Unity\\Hub\\Editor\\2021.3.5f1\\Editor\\Unity.exe\""
            env.unityProjectPath="\"${env.WORKSPACE}\""
            env.unityMethod='BatchBuild.BuildBatch'
        }

        for(platform in builderPlatforms){
            try {
                stage("ENV ${platform}"){
                    echo '( o_o)---- Add Unity Environment Variables...'
                    SetupUnityScriptEnvironmentVariables(platform)
                }

                // stage('License'){
                //     echo '( o_o)---- Activating unity license...'
                //     withCredentials([
                //         usernamePassword(credentialsId: 'UNITY_LICENSE1_USER', usernameVariable: 'USER', passwordVariable: 'PASS'),
                //         string(credentialsId: 'UNITY_LICENSE1_SERIAL', variable: 'UNITY_SERIAL'),
                //     ]){
                //         // https://docs.unity3d.com/Manual/ManagingYourUnityLicense.html
                //         bat '%unityExecutable% -quit -logfile - -batchmode -username %USER% -password %PASS% -serial %UNITY_SERIAL%'
                //     }
                // }

                stage('Artifact'){
                    echo '( o_o)---- Creating artifacts folder...'
                    bat "mkdir ${artifactsPath}"
                }

                // stage('Test'){
                //     echo "( o_o)---- Running Unity Test Framework PlayMode tests..."
                //     bat RunUnityPlaymodeTests()
                // }

                stage('Build'){
                    echo '( o_o)---- Building Unity Player...'
                    bat BuildUnityPlayerBatchmode()
                }

                // stage('Publish'){
                //     echo '( o_o)---- Publishing to app distribution...'
                    
                //     env.appPath = "${env.artifactsPath}\\${env.buildName}.aab"

                //     withCredentials([
                //         string(credentialsId: 'APP_DISTRIBUTION_ID', variable: 'APP_DISTRIBUTION_ID_VAR'),
                //         file(credentialsId: 'FIREBASE_GOOGLE_SERVICE_ACCOUNT', variable: 'GOOGLE_APPLICATION_CREDENTIALS'),
                //     ]){
                
                //         bat 'firebase appdistribution:distribute %appPath% --app %APP_DISTRIBUTION_ID_VAR% --testers felipe.monomyto@gmail.com --release-notes \"JENKINS BUILD %BUILD_NUMBER%\"'
                //     }
                // }
            }
            finally {
                // stage('End'){
                //     echo '( o_o)---- Registering nunit test reports...'
                //     nunit testResultsPattern: testsXMLReport, failedTestsFailBuild: true, failIfNoResults:true

                //     echo '( o_o)---- Returning Unity License...'
                //     withCredentials([usernamePassword(credentialsId: 'UNITY_LICENSE1_USER', usernameVariable: 'USER', passwordVariable: 'PASS')]){

                //         bat '%unityExecutable% -quit -logfile - -batchmode -returnlicense -username %USER% -password %PASS%'
                //     }
                // }
            }
        }
    }
}

//--------------------------------------------- MAC BUILDER
def MACOSBuilderPipeline(){
    echo 'starting build in MACOS agent...'
    def builderPlatforms = ['ios']

    node(GetAgentLabel('mac')){

        stage('Checkout'){
            echo "( o_o)---- Sync git repository..."
            checkout scm
        }

        stage('ENV Builder'){
            echo "( o_o)---- Add Builder Environment variables..."
            env.unityExecutable="~/Unity/Hub/Editor/2021.3.5f1/Editor/Unity"
            env.unityProjectPath="${env.WORKSPACE}"
            env.unityMethod='BatchBuild.BuildBatch'
        }

        for(platform in builderPlatforms){
            try {

                stage("ENV ${platform}"){
                    echo '( o_o)---- Add Unity Environment Variables...'
                    SetupUnityScriptEnvironmentVariablesMAC(platform)
                }

                stage('License'){
                    echo '( o_o)---- Activating unity license...'
                    withCredentials([
                        usernamePassword(credentialsId: 'UNITY_LICENSE2_USER', usernameVariable: 'USER', passwordVariable: 'PASS'),
                        string(credentialsId: 'UNITY_LICENSE2_SERIAL', variable: 'UNITY_SERIAL'),
                    ]){
                        // https://docs.unity3d.com/Manual/ManagingYourUnityLicense.html
                        // bat '%unityExecutable% -quit -logfile - -batchmode -username %USER% -password %PASS% -serial %UNITY_SERIAL%'
                    }
                }

                stage('Artifact'){
                    echo '( o_o)---- Creating artifacts folder...'
                    // bat "mkdir ${artifactsPath}"
                }

                stage('Test'){
                    echo "( o_o)---- Running Unity Test Framework PlayMode tests..."
                    // bat RunUnityPlaymodeTests()
                }

                stage('Build'){
                    echo '( o_o)---- Building Unity Player...'
                    // bat BuildUnityPlayerBatchmode()
                }

                stage('Build xcode'){
                    echo '( o_o)---- Building xcode project using fastlane'
                }

                stage('Publish'){
                    echo '( o_o)---- Publishing to app distribution...'
                    
                    env.appPath = "${env.artifactsPath}/${env.buildName}.ipa"

                    withCredentials([
                        string(credentialsId: 'APP_DISTRIBUTION_ID_IOS', variable: 'APP_DISTRIBUTION_ID_VAR'),
                        file(credentialsId: 'FIREBASE_GOOGLE_SERVICE_ACCOUNT', variable: 'GOOGLE_APPLICATION_CREDENTIALS'),
                    ]){
                
                        // bat 'firebase appdistribution:distribute %appPath% --app %APP_DISTRIBUTION_ID_VAR% --testers felipe.monomyto@gmail.com --release-notes \"JENKINS BUILD %BUILD_NUMBER%\"'
                    }
                }
            }
            finally {
                stage('End'){
                    echo '( o_o)---- Registering nunit test reports...'
                    nunit testResultsPattern: testsXMLReport, failedTestsFailBuild: true, failIfNoResults:true

                    echo '( o_o)---- Returning Unity License...'
                    withCredentials([usernamePassword(credentialsId: 'UNITY_LICENSE2_USER', usernameVariable: 'USER', passwordVariable: 'PASS')]){

                        // bat '%unityExecutable% -quit -logfile - -batchmode -returnlicense -username %USER% -password %PASS%'
                    }
                }
            }

        }
    }
}