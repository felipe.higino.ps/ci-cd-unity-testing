@REM dotnet pack
@REM dotnet tool install --global --add-source ./nupkg batchmode.cli
@REM dotnet tool update --global --add-source ./nupkg batchmode.cli
@REM dotnet tool uninstall --global batchmode.cli

@REM dotnet publish CICD-CLI/CLI/Batchmode.CLI -c release -o CICD-CLI/CLI/Batchmode.CLI/bin
@REM CICD-CLI/CLI/Batchmode.CLI/bin/Batchmode.CLI

dotnet run --project Batchmode.CLI
pause