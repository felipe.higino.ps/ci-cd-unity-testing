﻿using System;
using System.Diagnostics;

#pragma warning disable CS8604
// #pragma warning disable CS8618
// #pragma warning disable CS8601

public static class CLI
{
    static void Main()
    {
        System.Console.WriteLine("Hello, world!");

        string? unityExecutable = Environment.GetEnvironmentVariable("unityExecutable");
        string? unityProjectPath = Environment.GetEnvironmentVariable("unityProjectPath");
        string? unityMethod = Environment.GetEnvironmentVariable("unityMethod");

        IBatchmodeBuilder builder = new BatchmodeBuilder_Windows();

        var buildScript = builder
            .GetBuildScript(unityExecutable, unityProjectPath, unityMethod)
            .WithNoGraphics()
            // .WithLogs(@"""C:\Projetos Unity\Monomyto\gunstars-android-copy\MonomytoSystems\CICD\Tool\logs.logs""");
            .WithLogs('-');

        System.Console.WriteLine(buildScript);

        Process process = new Process();
        process.StartInfo = new ProcessStartInfo("cmd.exe", "/c " + $@"""{buildScript}""");
        process.Start();
        process.WaitForExit();
        process.Close();
        process.Dispose();
    }
}