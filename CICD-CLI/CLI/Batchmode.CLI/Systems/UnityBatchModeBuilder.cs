using System.Collections.Generic;
using System.Text;

public enum BuildOS
{
    MAC,
    LINUX,
    WINDOWS,
}

public enum Architecture
{
    /// <value>android 64bits & iOS</value>
    arm64_v8a,

    /// <value>android 32bits</value>
    armeabi_v7a,

    /// <value>chrome OS</value>
    x86,

    /// <value>chrome OS</value>
    x86_64,
}

public enum UnityBuildTargets
{
    Standalone,
    Win,
    Win64,
    OSXUniversal,
    Linux64,
    iOS,
    Android,
    WebGL,
    WindowsStoreApps,
    tvOS,
}

public enum BuildEnvironment
{
    DEV,
    PROD,
}

// UNITY CLI ARGUMENTS: https://docs.unity3d.com/Manual/EditorCommandLineArguments.html

public abstract class A_BatchModeStrategy
{
    public enum UnityBatchmodeFlags
    {
        /// <value>Quit batchmode when all operations concluded.</value>
        QUIT,

        /// <value>Main batchmode flag.</value>
        BATCH_MODE,

        /// <value>The path of your unity project.</value>
        PROJECT_PATH,

        /// <value>The method you will call on batch mode. The method must be static and can be private.</value>
        EXECUTE_METHOD,

        /// <value>Path where log file will be written.</value>
        LOG_FILE_PATH,

        /// <value>Unity doesn’t initialize the graphics device. You can then run automated workflows on machines that don’t have a GPU.</value>
        NO_GRAPHICS,

        /// <value>Select an active build target before loading a project</value>
        BUILD_TARGET,

        /// <value>Add credentials to license. Requires password & username (e-mail)</value>
        ACTIVATE_LICENSE,

        /// <value>Enter a password into the log-in form during the activation of the Unity Editor.</value>
        PASSWORD,

        /// <value>Enter a username into the log-in form during the activation of the Unity Editor.</value>
        USERNAME,
    }

    protected static Dictionary<UnityBatchmodeFlags, string> batchmodeFlags = new Dictionary<UnityBatchmodeFlags, string>
    {
        {UnityBatchmodeFlags.QUIT, "-quit"},
        {UnityBatchmodeFlags.BATCH_MODE, "-batchmode"},
        {UnityBatchmodeFlags.PROJECT_PATH, "-projectPath"},
        {UnityBatchmodeFlags.EXECUTE_METHOD, "-executeMethod"},
        {UnityBatchmodeFlags.LOG_FILE_PATH, "-logFile"},
        {UnityBatchmodeFlags.PASSWORD, "-password"},
        {UnityBatchmodeFlags.USERNAME, "-username"},
        {UnityBatchmodeFlags.NO_GRAPHICS, "-nographics"},
        {UnityBatchmodeFlags.BUILD_TARGET, "-buildTarget"},
        {UnityBatchmodeFlags.ACTIVATE_LICENSE, "-serial"},
    };
}