class BatchmodeBuilder_MAC : IBatchmodeBuilder
{
    const string mac_build_cli = "{0} -quit -batchmode -projectPath {1} -executeMethod {2}";

    public string GetBuildScript(
        string unityExecutable = "/Applications/Unity/Unity.app/Contents/MacOS/Unity",
        string projectPath = "~/UnityProjects/MyProject",
        string unityMethod = "MyEditorScript.PerformBuild")
    {
        return string.Format(mac_build_cli, unityExecutable, projectPath, unityMethod);
    }
}