interface IBatchmodeBuilder
{
    /// <summary>
    /// Build script to execute in batchmode
    /// </summary>
    /// <param name="unityExecutable">unity.exe path</param>
    /// <param name="projectPath">your project path</param>
    /// <param name="unityMethod">the method batch mode will call. Must be a static method implemented in unity</param>
    string GetBuildScript(
        string unityExecutable,
        string projectPath,
        string unityMethod);
}

public static class IBatchmodeBuilderExtensions
{
    public static string WithLogs(this string buildScript, string logPath)
    {
        buildScript += $" -logFile {logPath}";
        return buildScript;
    }

    public static string WithNoGraphics(this string buildScript)
    {
        buildScript += " -nographics";
        return buildScript;
    }

    public static string WithBuildTarget(this string buildScript, UnityBuildTargets buildTarget)
    {
        buildScript += $" -buildTarget {buildTarget.ToString()}";
        return buildScript;
    }

    public enum UnityBuildTargets
    {
        Standalone,
        Win,
        Win64,
        OSXUniversal,
        Linux64,
        iOS,
        Android,
        WebGL,
        WindowsStoreApps,
        tvOS,
    }
}