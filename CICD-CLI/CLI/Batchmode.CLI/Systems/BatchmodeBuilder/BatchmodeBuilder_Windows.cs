class BatchmodeBuilder_Windows : IBatchmodeBuilder
{
    const string win_build_cli = @"{0} -quit -batchmode -projectPath {1} -executeMethod {2}";

    public string GetBuildScript(string unityExecutable, string projectPath, string unityMethod)
    {
        return string.Format(win_build_cli, unityExecutable, projectPath, unityMethod);
    }
}