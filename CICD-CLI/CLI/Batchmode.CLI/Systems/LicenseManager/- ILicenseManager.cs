// https://docs.unity3d.com/Manual/ManagingYourUnityLicense.html#license-activation-cli

public interface ILicenseManager
{
    /// <param name="unityPath">unity.exe path</param>
    /// <param name="username">username (name@example.com) </param>
    /// <param name="password">password (XXXXXXXXXXXXX)</param>
    /// <param name="serial">serial (SB-XXXX-XXXX-XXXX-XXXX-XXXX)</param>
    string ActivateLicenseCommand(string unityPath, string username, string password, string serial);

    /// <param name="unityPath">unity.exe path</param>
    /// <param name="username">username (name@example.com) </param>
    /// <param name="password">password (XXXXXXXXXXXXX)</param>
    string ReturnLicenseCommand(string unityPath, string username, string password);
}
