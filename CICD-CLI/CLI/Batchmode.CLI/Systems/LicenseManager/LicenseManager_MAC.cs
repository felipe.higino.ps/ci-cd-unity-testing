class LicenseManager_MAC : ILicenseManager
{
    const string mac_activation = "-quit -batchmode -serial {3} -username '{1}' -password '{2}' -nographics";
    const string mac_deactivation = "{0} -quit -batchmode -returnlicense -username '{1}' -password '{2}'";

    string ILicenseManager.ActivateLicenseCommand(string unityPath, string username, string password, string serial)
    {
        return string.Format(mac_activation, unityPath, username, password, serial);
    }

    string ILicenseManager.ReturnLicenseCommand(string unityPath, string username, string password)
    {
        return string.Format(mac_activation, unityPath, username, password);
    }
}