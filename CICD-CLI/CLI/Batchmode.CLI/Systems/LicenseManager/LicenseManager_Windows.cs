class LicenseManager_Windows : ILicenseManager
{
    const string windows_activation = @"""{0}"" -batchmode -username {1} -password {2} -serial {3} –quit";
    const string windows_deactivation = @"""{0}"" -quit -batchmode -returnlicense -username {1} -password {2}";

    string ILicenseManager.ActivateLicenseCommand(string unityPath, string username, string password, string serial)
    {
        return string.Format(windows_activation, unityPath, username, password, serial);
    }

    string ILicenseManager.ReturnLicenseCommand(string unityPath, string username, string password)
    {
        return string.Format(windows_activation, unityPath, username, password);
    }
}