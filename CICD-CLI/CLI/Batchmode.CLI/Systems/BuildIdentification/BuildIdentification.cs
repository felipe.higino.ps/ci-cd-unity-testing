class BuildIdentification
{
    // "Gunstars_PROD_v0.0.0_android64_18-10-22_19-46";
    const string template = "Gunstars_{0}_v{1}_{2}_{3}";


    /// <param name="buildVersion">v0.0.0</param>
    /// <param name="targetPlatform">android64, android32, iOS, Windows</param>
    /// <param name="environment">DEV, PROD</param>
    /// <returns></returns>
    public static string GetBuildIdentification(string environment, string buildVersion, string targetPlatform)
    {
        string date = DateTime.UtcNow.ToString("dd-MM-yy_HH-mm");
        return string.Format(template, environment, buildVersion, targetPlatform, date);
    }
}