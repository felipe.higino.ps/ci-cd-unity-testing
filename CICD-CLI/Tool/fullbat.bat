set unityExecutable="C:\Program Files\Unity\Hub\Editor\2021.3.5f1\Editor\Unity.exe"
set projectPath="C:\Projetos Unity\Monomyto\ci-cd-unity-test"
set unityMethod=SimpleBuild.SayHello
set logFile="C:\Projetos Unity\Monomyto\gunstars-android-copy\MonomytoSystems\CICD\Tool\logs.logs"

%unityExecutable%^
 -quit^
 -batchmode^
 -projectPath %projectPath%^
 -executeMethod %unityMethod%^
 -logFile %logFile%

pause
