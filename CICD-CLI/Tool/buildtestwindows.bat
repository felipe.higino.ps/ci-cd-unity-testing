set unityExecutable="C:\Program Files\Unity\Hub\Editor\2021.3.5f1\Editor\Unity.exe"
set projectPath="C:\Projetos Unity\Monomyto\ci-cd-unity-test"
set unityMethod=SimpleBuild.SayHello

:: ------- BUILD CLI
cd "C:\Projetos Unity\Monomyto\gunstars-android-copy\MonomytoSystems\CICD\CLI"
call build.bat

:: ------- EXECUTE CLI
cd "C:\Projetos Unity\Monomyto\gunstars-android-copy\MonomytoSystems\CICD\CLI\bin"
call Core